#!/bin/bash

generate_password() {
    local length=$1
    local allowed_chars=$2
    local password=""
    local max_index=$(( ${#allowed_chars} - 1 ))

    for ((i = 0; i < length; i++)); do
        local random_index=$((RANDOM % max_index))
        password+="${allowed_chars:random_index:1}"
    done

    echo "$password"
}

options=$(yad --form \
    --title "Gerador de Senhas" \
    --text "Escolha as opções para gerar a senha" \
    --field "Tamanho da Senha" \
    --field "Letras Maiúsculas:CHK" \
    --field "Letras Minúsculas:CHK" \
    --field "Números:CHK" \
    --field "Caracteres Especiais:CHK"
)

password_length=$(echo "$options" | cut -d"|" -f1)
use_uppercase=$(echo "$options" | cut -d"|" -f2)
use_lowercase=$(echo "$options" | cut -d"|" -f3)
use_numbers=$(echo "$options" | cut -d"|" -f4)
use_special_chars=$(echo "$options" | cut -d"|" -f5)

allowed_chars=""
if [ "$use_uppercase" = "TRUE" ]; then
    allowed_chars+="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
fi

if [ "$use_lowercase" = "TRUE" ]; then
    allowed_chars+="abcdefghijklmnopqrstuvwxyz"
fi

if [ "$use_numbers" = "TRUE" ]; then
    allowed_chars+="0123456789"
fi

if [ "$use_special_chars" = "TRUE" ]; then
    allowed_chars+="!@#\$%^&*()"
fi

if [ -z "$allowed_chars" ]; then
    yad --error \
        --title "Erro" \
        --text "Selecione pelo menos uma opção de caracteres."
    exit 1
fi

password1=$(generate_password "$password_length" "$allowed_chars" | sed 's/&/&amp;/g')
password2=$(generate_password "$password_length" "$allowed_chars" | sed 's/&/&amp;/g')
password3=$(generate_password "$password_length" "$allowed_chars" | sed 's/&/&amp;/g')

# Embaralhar as senhas geradas
shuffled_passwords=$(printf "%s\n%s\n%s\n" "$password1" "$password2" "$password3" | shuf)

# Selecionar as três primeiras linhas (senhas)
selected_passwords=$(echo "$shuffled_passwords" | head -n 3)

# Filtrar senhas vazias
filtered_passwords=$(echo "$selected_passwords" | grep -v '^$')

yad --info \
    --title "Senhas Geradas" \
    --text "$(echo "$filtered_passwords" | sed 's/\n/\\n/g')"




